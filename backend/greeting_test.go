package greeting

import "testing"

func TestGreetOneName(t *testing.T) {
	t.Run("hello Bob", func(t *testing.T) {
		given := "Bob"
		expect := "Hello, Bob."
		result := greet(given)

		if expect != result {
			t.Errorf("%v should returns %v but get %v\n", given, expect, result)
		}
	})

	t.Run("hello Nan", func(t *testing.T) {
		given := "Nan"
		expect := "Hello, Nan."
		result := greet(given)

		if expect != result {
			t.Errorf("%v should returns %v but get %v\n", given, expect, result)
		}
	})

	t.Run("hello Flame", func(t *testing.T) {
		given := "Flame"
		expect := "Hello, Flame."
		result := greet(given)

		if expect != result {
			t.Errorf("%v should returns %v but get %v\n", given, expect, result)
		}
	})

	t.Run("hello World", func(t *testing.T) {
		given := "World"
		expect := "Hello, World."
		result := greet(given)

		if expect != result {
			t.Errorf("%v should returns %v but get %v\n", given, expect, result)
		}
	})
	t.Run("hello my friend", func(t *testing.T) {
		expect := "Hello, my friend."
		result := greet()

		if expect != result {
			t.Errorf("should returns %v but get %v\n", expect, result)
		}
	})

	t.Run("hello JERRY", func(t *testing.T) {
		given := "JERRY"
		expect := "HELLO, JERRY!"
		result := greet(given)

		if expect != result {
			t.Errorf("%v should returns %v but get %v\n", given, expect, result)
		}
	})
	t.Run("hello many friend", func(t *testing.T) {
		given := []string{"Amy", "Brian", "Charlotte"}
		expect := "Hello, Amy, Brian, and Charlotte."
		result := greet(given...)

		if expect != result {
			t.Errorf("%v should returns %v but get %v\n", given, expect, result)
		}
	})
}
