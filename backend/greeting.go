package greeting

import (
	"fmt"
	"strings"
)

func greet(names ...string) string {

	if len(names) == 0 {
		return fmt.Sprintf("Hello, %s.", "my friend")
	}

	if len(names) > 2 {
		result := "Hello"
		for i := 0; i < len(names); i++ {
			if i == len(names)-1 {
				result += ", and " + names[i] + "."
			} else {
				result += ", " + names[i]
			}
		}
		return result
	}

	if strings.ToUpper(names[0]) == names[0] {
		return fmt.Sprintf("HELLO, %s!", names[0])
	}

	return fmt.Sprintf("Hello, %s.", names[0])

}
