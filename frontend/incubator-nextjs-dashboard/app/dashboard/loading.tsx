import { Suspense } from "react";
import DashboardSkeleton from "../ui/skeletons";

export default function Loading() {
    return <DashboardSkeleton />;
    // <div>"Loading....."</div>;
    // return <Suspense fallback={<DashboardSkeleton />}></Suspense>
}
